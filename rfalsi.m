function [x,fx,er] = rfalsi(x0,x1,f)
x = [];
fx = [];
er = [];
error = 1;
while(error>0.0001)
    x2 = (x1 - (((x1-x0)*subs(f,x1))/(subs(f,x1)-subs(f,x0))));
    x = [x;x2];
    fx = [fx;subs(f,x2)];
    sign = subs(f,x1)*subs(f,x0);
    if(sign>0)
        x1 = x2;
        error = abs(x1-x0);
        er = [er;error];
    elseif(sign<0)
        x0 = x2;
        error = abs(x1-x0);
        er = [er;error];
    else
        error = -1;
    end
end
end