function [xf,fx,er] = ptofijo(x0,f)
xf = [];
fx = [];
er = [];
syms x;
g = f+x;
h = diff(g);
if(abs(subs(h,x0))>1)
    disp('diverge');
    return ;
else
    error = 1;
    while(error>0.0001)
        save = x0;
        x0 = subs(g,x0);
        xf = [xf;x0];
        fx = [fx;subs(f,x0)];
        error = abs(save-x0);
        er = [er;error];
    end
end