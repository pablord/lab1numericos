function [x,fx,er] = biseccion(f,a,b)
x = [];
fx = [];
er = [];
n=1;
if(sign(subs(f,a))==sign(subs(f,b)))
    disp('Los signos son iguales');
    return ;  
else
    while(abs(b-a)>0.0001)
        er = [er;abs(b-a)];
        disp(['iteracion: ',num2str(n)]);
        eva = subs(f,a);
        medio = (a+b)/2;
        evm = subs(f,medio);
        x = [x;medio];
        fx = [fx;evm]
        if(sign(evm) == sign(eva))
            a=medio;
            n=n+1;
        else
            b=medio;
            n=n+1;
        end
    end
end
    