function [x, fx, er] = mnr(x0,f)
x = [];
fx = [];
er = [];
err = 1;
g = diff(f);
while(err>0.0001)
    x = [x;x0];
    fx = [fx;subs(f,x0)];
    x1 = (x0 - (subs(f,x0)/subs(g,x0)));
    err  = abs((x1-x0)/x1);
    er = [er;err];
    x0=x1;
end