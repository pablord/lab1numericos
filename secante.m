function [x,fx,er] = secante(x0,x1,f)
x = [];
fx = [];
er = [];
error = 1;
er = [er;error];
while(error>0.0001)
    x = [x;x1];
    fx = [x;subs(f,x1)];
    x2 = (x1 - (((x1-x0)*subs(f,x1))/(subs(f,x1)-subs(f,x0))));
    x0=x1;
    x1=x2;
    error = abs(x1-x0);
    er = [er;error];
end